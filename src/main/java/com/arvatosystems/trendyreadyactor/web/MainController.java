package com.arvatosystems.trendyreadyactor.web;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

	@GetMapping(value = { "", "/" })
	public String main() {
		return "redirect:/search";
	}
	
	@GetMapping(value = { "/search" })
	public String search() {
		return "search";
	}
	
	@RequestMapping(value = { "/analysis" }, method= {RequestMethod.GET, RequestMethod.POST})
	public String analysis(Model model, @RequestParam Map<String, Object> requestParams) {
		return "analysis";
	}
}
